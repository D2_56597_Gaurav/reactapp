import axios from 'axios';

const base_url = 'http://healthclubmanagement-env.eba-9hqu3e2s.us-east-1.elasticbeanstalk.com:5000';

const LOGIN_API_URL = `${base_url}/login`;

class ApiService {

    login(data) {
        return axios.post(LOGIN_API_URL, data);
    }

}

export default new ApiService;