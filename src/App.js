import React, {useState} from "react";
import Navbar from "./Components/Navbar";
import Header from "./Components/Header";
import Feature from "./Components/Feature";
import Offer from "./Components/Offer";
import About from "./Components/About";
import Contact from "./Components/Contact";
import Login from "./Components/Login";
import SignUp from "./Components/SignUp";
import TrainerSignUp from "./Components/TrainerSignUp";
import MemberSignUp from "./Components/MemberSignUp";
import Admin from "./Components/Admin";
import Trainer from "./Components/Trainer";
import Member from "./Components/Member";
import AllPersons from "./Components/AllPersons";
import AllTrainers from "./Components/AllTrainers";
import AllMembers from "./Components/AllMembers";
import Profile from "./Components/Profile";
import Workout from "./Components/Workout";
import Diet from "./Components/Diet";
import AssignDiet from "./Components/AssignDiet";
import AssignWorkout from "./Components/AssignWorkout";
import DietInfo from "./Components/DietInfo";
import WorkoutInfo from "./Components/WorkoutInfo";
import TrainerEditInfo from "./Components/TrainerEditInfo";
import MemberEditInfo from "./Components/MemberEditInfo";
import EditInfo from "./Components/EditInfo";
import EditWorkout from "./Components/EditWorkout";
import EditDiet from "./Components/EditDiet";
import { UserContext } from './Components/UserContext.js';


import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

function App() {

  const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')));

  return (
    <div className="App">
      
      
      <Router>
      <UserContext.Provider value = {{user, setUser}}>
      <div class="row">
      <Navbar />
      </div>
      <div class="row">
      <Routes>
            <Route path="/" element={<Header />} exact />
            <Route path="/features" element={<Feature />} exact />
            <Route path="/offer" element={<Offer />} exact />
            <Route path="/about" element={<About />} exact />
            <Route path="/contact" element={<Contact />} exact />
            <Route path="/login" element={<Login />} exact />
            <Route path="/signup" element={<SignUp />} exact />
            <Route path="/trainersignup" element={<TrainerSignUp />} exact />
            <Route path="/membersignup" element={<MemberSignUp />} exact />
            <Route path="/admin" element={<Admin />} exact />
            <Route path="/trainer" element={<Trainer />} exact />
            <Route path="/member" element={<Member />} exact />
            <Route path="/allpersons" element={<AllPersons />} exact />
            <Route path="/alltrainers" element={<AllTrainers />} exact />
            <Route path="/allmembers" element={<AllMembers />} exact />
            <Route path="/profile" element={<Profile />} exact />
            <Route path="/workout" element={<Workout />} exact />
            <Route path="/assigndiet" element={<AssignDiet />} exact />
            <Route path="/assignworkout" element={<AssignWorkout />} exact />
            <Route path="/diet" element={<Diet />} exact />
            <Route path="/dietinfo" element={<DietInfo />} exact />
            <Route path="/workoutinfo" element={<WorkoutInfo />} exact />
            <Route path="/trainereditinfo/:id" element={<TrainerEditInfo />} exact />
            <Route path="/membereditinfo/:id" element={<MemberEditInfo />} exact />
            <Route path="/editinfo/:id" element={<EditInfo />} exact />
            <Route path="/editworkout/:id" element={<EditWorkout />} exact />
            <Route path="/editdiet/:id" element={<EditDiet />} exact />
      </Routes>
      </div>
      </UserContext.Provider>
      </Router>
      </div>
  );
}

export default App;
