import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import base_url from "../api/bootapi";

function MemberSignUp() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };
  const navigate = useNavigate();
  const [person, setPerson] = useState({});

  if (x > 0) {
    return dashboard();
  } else {
  const postDatatoServer = (data) => {
    console.log("response data");
    console.log(data.name);

    axios.post(`${base_url}/saveperson`, data).then(
      (response) => {
        console.log(response.join_date);
        console.log("success");
        toast.success("Persons added successfully", {
          position: "bottom-center",
        });
        setPerson({
          id: "",
          name: "",
          gender: "",
          dob: "",
          age: "",
          mobile: "",
          email: "",
          address: "",
          username: "",
          password: "",
          role: "",
          height: "",
          weight: "",
          join_date: "",
          end_date: "",
        });
      },
      (error) => {
        console.log(error);
        console.log("error");
        toast.error("Something went wrong", {
          position: "bottom-center",
        });
      }
    );
  };

  
  //form handler function
  const handleForm = (e) => {
    
    console.log(person);
    console.log(person.id);
    console.log(person.name);

    const jsonData = {
      name: person.name,
      gender: person.gender,
      dob: person.dob,
      age: person.age,
      mobile: person.mobile,
      email: person.email,
      address: person.address,
      username: person.username,
      password: person.password,
      role: person.role,
      member: {
        height: person.height,
        weight: person.weight,
        join_date: person.join_date,
        end_date: person.end_date,
      },
    };

    postDatatoServer(jsonData);
    alert("Registeration Succesfull..!!");
    e.preventDefault();
    navigate("/login");
  };

  return (
    <div id="signup">
      <h1>
        <span>Regi</span>ster
      </h1>
      <form onSubmit={handleForm}>
        <div>
          <label className="details">Name</label>
          <input
            type="text"
            placeholder="Name"
            required
            id="name"
            onChange={(e) => {
              setPerson({ ...person, name: e.target.value });
            }}
          />

          <label className="details">Gender</label>
          <select
            id="gender"
            onChange={(e) => {
              setPerson({ ...person, gender: e.target.value });
            }}
          >
            <option></option>
            <option>M</option>
            <option>F</option>
          </select>

          <label className="details">DOB</label>
          <input
            type="date"
            placeholder="DOB"
            required
            id="dob"
            onChange={(e) => {
              setPerson({ ...person, dob: e.target.value });
            }}
          />

          <label className="details">Age</label>
          <input
            type="number"
            placeholder="Age"
            required
            id="age"
            onChange={(e) => {
              setPerson({ ...person, age: e.target.value });
            }}
          />

          <label className="details">Mobile Number</label>
          <input
            type="number"
            placeholder="Mobile Number"
            pattern="[0-9]{10}"
            required
            id="mobile"
            onChange={(e) => {
              setPerson({ ...person, mobile: e.target.value });
            }}
          />

          <label className="details">Email</label>
          <input
            type="email"
            placeholder="Enter email"
            pattern="/[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g"
            required
            id="email"
            onChange={(e) => {
              setPerson({ ...person, email: e.target.value });
            }}
          />

          <label className="details">Address</label>
          <input
            type="text"
            placeholder="Address"
            required
            id="address"
            onChange={(e) => {
              setPerson({ ...person, address: e.target.value });
            }}
          />

          <label className="details">Username</label>
          <input
            type="text"
            placeholder="Username"
            required
            id="username"
            onChange={(e) => {
              setPerson({ ...person, username: e.target.value });
            }}
          />

          <label className="details">Password</label>
          <input
            type="password"
            placeholder="Enter password"
            required
            id="password"
            onChange={(e) => {
              setPerson({ ...person, password: e.target.value });
            }}
          />

          <label className="details">Role</label>
          <select
            id="role"
            onChange={(e) => {
              setPerson({ ...person, role: e.target.value });
            }}
          >
            <option></option>
            <option>member</option>
          </select>

          <label className="details">Height</label>
          <input
            type="number"
            step=".01"
            placeholder="Height"
            required
            id="height"
            onChange={(e) => {
              setPerson({ ...person, height: e.target.value });
            }}
          />

          <label className="details">Weight (in Kg)</label>
          <input
            type="number"
            step=".01"
            placeholder="Weight (in Kg)"
            required
            id="weight"
            onChange={(e) => {
              setPerson({ ...person, weight: e.target.value });
            }}
          />

          <label className="details">Join Date</label>
          <input
            type="date"
            placeholder="Join Date"
            required
            id="joindate"
            onChange={(e) => {
              setPerson({ ...person, join_date: e.target.value });
            }}
          />

          <label className="details">End Date</label>
          <input
            type="date"
            placeholder="End Date"
            required
            id="enddate"
            onChange={(e) => {
              setPerson({ ...person, end_date: e.target.value });
            }}
          />
        </div>

        <input type="submit" value="Register" />

        <p className="details">
          Already registered? <Link to="/login">LogIn</Link>
        </p>
      </form>
    </div>
  );
  }
  
}

export default MemberSignUp;
