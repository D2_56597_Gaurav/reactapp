import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import base_url from "../api/bootapi";

function TrainerEditInfo() {
  let userDetails = JSON.parse(localStorage.getItem('user'));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };


  const [person, setPerson] = useState([]);
  const [trainer, setTrainer] = useState([]);
  const navigate = useNavigate();
  const params = useParams();

  useEffect(() => {
    const fetchData = () => {
      axios.get(`${base_url}/getperson/${params.id}`).then(
        (response) => {
          setPerson(response.data[0]);
          setTrainer(response.data[0].trainer);
          // setQualification(response.data[0].trainer.qualification);
          console.log(response.data);
          console.log(person);
          console.log(trainer);
          // console.log(JSON.stringify(person).name);
          // console.log(person[0].name);
        },
        (error) => {
          //for error
          console.log(error);
        }
      );
    };
    fetchData();
    // console.log("person"+ person)
  }, []);

  const handleForm = (e) => {
    e.preventDefault();
    // console.log(person);
    // console.log(person.id);
    // console.log(person.name);
    // console.log(person.trainer.qualification);
    // debugger;

    const jsonData = {
      id: person.id,
      name: person.name,
      gender: person.gender,
      dob: person.dob,
      age: person.age,
      mobile: person.mobile,
      email: person.email,
      address: person.address,
      username: person.username,
      password: person.password,
      role: person.role,
      trainer: {
        trainer_id: person.trainer.trainer_id,
        qualification: trainer.qualification,
        specialization: trainer.specialization,
        college: trainer.college,
        marks: trainer.marks,
        salary: trainer.salary,
      },
    };

    postDatatoServer(jsonData);
    alert("Update Succesfull..!!");
    navigate("/profile");
  };

  const postDatatoServer = (data) => {
    console.log("response data");
    console.log(data);

    axios.put(`${base_url}/saveperson`, data).then(
      (response) => {
        console.log("success");
        setPerson({
          name: "",
          gender: "",
          dob: "",
          age: "",
          mobile: "",
          address: "",
          username: "",
          qualification: "",
          specialization: "",
          college: "",
          marks: "",
          salary: "",
        });
      },
      (error) => {
        console.log(error);
        console.log("error");
      }
    );
  };

  if (x > 0) {
    if (userDetails.role == "trainer") {
      return (
        <div id="signup">
          <h1>
            <span>Edit </span>Info
          </h1>
          <form onSubmit={handleForm}>
            <div>
              <label className="details">Name</label>
              <input
                type="text"
                id="name"
                value={person.name}
                onChange={(e) => {
                  setPerson({ ...person, name: e.target.value });
                }}
              />
    
              <label className="details">Gender</label>
              <select
                id="gender"
                value={person.gender}
                onChange={(e) => {
                  setPerson({ ...person, gender: e.target.value });
                }}
              >
                <option></option>
                <option>M</option>
                <option>F</option>
              </select>
    
              <label className="details">DOB</label>
              <input
                type="date"
                value={person.dob}
                id="dob"
                onChange={(e) => {
                  setPerson({ ...person, dob: e.target.value });
                }}
              />
    
              <label className="details">Age</label>
              <input
                type="number"
                value={person.age}
                id="age"
                onChange={(e) => {
                  setPerson({ ...person, age: e.target.value });
                }}
              />
    
              <label className="details">Mobile Number</label>
              <input
                type="tel"
                value={person.mobile}
                id="mobile"
                pattern="[0-9]{10}"
                onChange={(e) => {
                  setPerson({ ...person, mobile: e.target.value });
                }}
              />
    
              <label className="details">Address</label>
              <input
                type="text"
                value={person.address}
                id="address"
                onChange={(e) => {
                  setPerson({ ...person, address: e.target.value });
                }}
              />
    
              <label className="details">Username</label>
              <input
                type="text"
                value={person.username}
                id="username"
                onChange={(e) => {
                  setPerson({ ...person, username: e.target.value });
                }}
              />
    
              <label className="details">Qualification</label>
              <input
                type="text"
                value={trainer.qualification}
                id="qualification"
                onChange={(e) => {
                  setTrainer({ ...trainer, qualification: e.target.value });
                }}
              />
    
              <label className="details">Specialization</label>
              <input
                type="text"
                value={trainer.specialization}
                id="specialization"
                onChange={(e) => {
                  setTrainer({ ...trainer, specialization: e.target.value });
                }}
              />
    
              <label className="details">College</label>
              <input
                type="text"
                value={trainer.college}
                id="college"
                onChange={(e) => {
                  setTrainer({ ...trainer, college: e.target.value });
                }}
              />
    
              <label className="details">Marks</label>
              <input
                type="number"
                value={trainer.marks}
                id="marks"
                onChange={(e) => {
                  setTrainer({ ...trainer, marks: e.target.value });
                }}
              />
    
              <label className="details">Salary</label>
              <input
                type="number"
                value={trainer.salary}
                id="salary"
                onChange={(e) => {
                  setTrainer({ ...trainer, salary: e.target.value });
                }}
              />
    
              <input type="submit" value="Update"  />
              {/* <input type="submit" value="Back" /> */}
            </div>
            <div style={{ paddingTop: "5px"}}>
                <Link to="/profile" className="pr-btn">
                  Back
                </Link>
              </div>
          </form>
        </div>
      );

    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default TrainerEditInfo;
