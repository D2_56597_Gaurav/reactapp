import React from "react";

const Profilelayout = ({ person, update }) => {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  if (userDetails.role == "member") {
    return (
      <tr>
        <td>{person.id}</td>
        <td>{person.name}</td>
        <td>{person.gender}</td>
        <td>{person.dob}</td>
        <td>{person.age}</td>
        <td>{person.mobile}</td>
        <td>{person.email}</td>
        <td>{person.address}</td>
        <td>{person.username}</td>
        <td>{person.member.member_id}</td>
        <td>{person.member.height}</td>
        <td>{person.member.weight}</td>
        <td>{person.member.join_date}</td>
        <td>{person.member.end_date}</td>
      </tr>
    );
  } else if (userDetails.role == "trainer") {
  
    return (
      <tr>
        <td>{person.id}</td>
        <td>{person.name}</td>
        <td>{person.gender}</td>
        <td>{person.dob}</td>
        <td>{person.age}</td>
        <td>{person.mobile}</td>
        <td>{person.email}</td>
        <td>{person.address}</td>
        <td>{person.username}</td>
        <td>{person.trainer.trainer_id}</td>
        <td>{person.trainer.qualification}</td>
        <td>{person.trainer.specialization}</td>
        <td>{person.trainer.college}</td>
        <td>{person.trainer.marks}</td>
        <td>{person.trainer.salary}</td>
      </tr>
    );
  }
};

export default Profilelayout;
