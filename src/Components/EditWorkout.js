import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import base_url from "../api/bootapi";
import { Link, useParams } from "react-router-dom";

function EditWorkout() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const params = useParams();

  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  const [workout, setWorkout] = useState({});
  useEffect(() => {
    const fetchData = () => {
      axios.get(`${base_url}/getworkout/${params.id}`).then(
        (response) => {
          setWorkout(response.data[0]);
          console.log(workout);
        },
        (error) => {
          console.log(error);
        }
      );
    };
    fetchData();
    // console.log("person"+ person)
  }, []);

  if (x > 0) {
    if (userDetails.role == "trainer") {
      const postDatatoServer = (data) => {
        console.log("response data");
        console.log(data.name);

        axios.put(`${base_url}/saveUpdateWorkout`, data).then(
          (response) => {
            console.log("success");
            toast.success("Workout added successfully", {
              position: "bottom-center",
            });
            setWorkout({
              workout_id: "",
              trainer_id: "",
              member_id: "",
              workoutType: "",
            });
          },
          (error) => {
            console.log(error);
            console.log("error");
            toast.error("Something went wrong", {
              position: "bottom-center",
            });
          }
        );
      };

      //form handler function
      const handleForm = (e) => {
        console.log(workout);
        console.log(workout.workout_id);
        postDatatoServer(workout);
        e.preventDefault();
        window.location.href = "/workout";
      };

      return (
        <div id="signup" style={{ height: "100vh", paddingTop: "5%" }}>
          <h1>
            <span>Update </span>Workout
          </h1>
          <form onSubmit={handleForm}>
            <div>
              <label className="details">Workout Id</label>
              <input
                type="number"
                id="workout_id"
                placeholder={workout.workout_id}
                readOnly
              />
              <label className="details">Trainer Id</label>
              <input
                type="number"
                id="trainer_id"
                placeholder={workout.trainer_id}
                readOnly
              />

              <label className="details">Member Id</label>
              <input
                type="number"
                id="member_id"
                value={workout.member_id}
                onChange={(e) => {
                  setWorkout({ ...workout, member_id: e.target.value });
                }}
              />

              <label className="details">Workout Type</label>
              <select
                id="workoutType"
                placeholder={workout.workoutType}
                onChange={(e) => {
                  setWorkout({ ...workout, workoutType: e.target.value });
                }}
              >
                <option>{workout.workoutType}</option>
                <option>Type 1( warmup + pushup + cardio 5min)</option>
                <option>
                  Type 2(warmup + pushup +upperbody+ cardio 10 min)
                </option>
                <option>
                  Type 3(warmup + pushup +upperbody+ lowerbody+ cardio 10 min)
                </option>
                <option>Type 4(warmup + 1 day 3 parts + cardio 15 min)</option>
              </select>

              <input type="submit" value="Update Workout" />
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="/workoutinfo" className="pr-btn">
                Back
              </Link>
            </div>
          </form>
        </div>
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default EditWorkout;
