import React from 'react';
import { Link } from "react-router-dom";

function Offer() {
    return (
        <div id='offer'>
            <div className='pr-heading'>
                <h1>A BIG <span>SALE</span> ON THIS SUMMER</h1>
                <p className='details'>Buy one get one free</p>
                <div className='pr-btns'>
                    <Link to="/signup" className='pr-btn'>JOIN NOW</Link>
                </div>
            </div>
        </div>
    )
}

export default Offer;
