import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import base_url from "../api/bootapi";

function MemberEditInfo() {
  const [person, setPerson] = useState([]);
  const [member, setMember] = useState([]);
  const navigate = useNavigate();
  const params = useParams();

  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  useEffect(() => {
    const fetchData = () => {
      axios.get(`${base_url}/getperson/${params.id}`).then(
        (response) => {
          setPerson(response.data[0]);
          setMember(response.data[0].member);
          console.log(response.data);
          console.log(person);
          console.log(member);
        },
        (error) => {
          console.log(error);
        }
      );
    };
    fetchData();
    // console.log("person"+ person)
  }, []);

  const handleForm = (e) => {
    e.preventDefault();

    const jsonData = {
      id: person.id,
      name: person.name,
      gender: person.gender,
      dob: person.dob,
      age: person.age,
      mobile: person.mobile,
      email: person.email,
      address: person.address,
      username: person.username,
      password: person.password,
      role: person.role,
      member: {
        member_id: person.member.member_id,
        height: member.height,
        weight: member.weight,
        join_date: member.join_date,
        end_date: member.end_date,
      },
    };

    postDatatoServer(jsonData);
    alert("Update Succesfull..!!");
    navigate("/profile");
  };

  const postDatatoServer = (data) => {
    console.log("response data");
    console.log(data);

    axios.put(`${base_url}/saveperson`, data).then(
      (response) => {
        console.log("success");
        setPerson({
          name: "",
          gender: "",
          dob: "",
          age: "",
          mobile: "",
          address: "",
          username: "",
          height: "",
          weight: "",
          join_date: "",
          end_date: "",
        });
      },
      (error) => {
        console.log(error);
        console.log("error");
      }
    );
  };

  if (x > 0) {
    if (userDetails.role == "member") {
      return (
        <div id="signup">
          <h1>
            <span>Edit </span>Info
          </h1>
          <form onSubmit={handleForm}>
            <div>
              <label className="details">Name</label>
              <input
                type="text"
                id="name"
                value={person.name}
                onChange={(e) => {
                  setPerson({ ...person, name: e.target.value });
                }}
              />
    
              <label className="details">Gender</label>
              <select
                id="gender"
                value={person.gender}
                onChange={(e) => {
                  setPerson({ ...person, gender: e.target.value });
                }}
              >
                <option></option>
                <option>M</option>
                <option>F</option>
              </select>
    
              <label className="details">DOB</label>
              <input
                type="date"
                value={person.dob}
                id="dob"
                onChange={(e) => {
                  setPerson({ ...person, dob: e.target.value });
                }}
              />
    
              <label className="details">Age</label>
              <input
                type="number"
                value={person.age}
                id="age"
                onChange={(e) => {
                  setPerson({ ...person, age: e.target.value });
                }}
              />
    
              <label className="details">Mobile Number</label>
              <input
                type="tel"
                value={person.mobile}
                id="mobile"
                pattern="[0-9]{10}"
                onChange={(e) => {
                  setPerson({ ...person, mobile: e.target.value });
                }}
              />
    
              <label className="details">Address</label>
              <input
                type="text"
                value={person.address}
                id="address"
                onChange={(e) => {
                  setPerson({ ...person, address: e.target.value });
                }}
              />
    
              <label className="details">Username</label>
              <input
                type="text"
                value={person.username}
                id="username"
                onChange={(e) => {
                  setPerson({ ...person, username: e.target.value });
                }}
              />
              
              <label className="details">Height</label>
              <input
                type="number"
                step=".01"
                value={member.height}
                id="height"
                onChange={(e) => {
                  setMember({ ...member, height: e.target.value });
                }}
              />
    
              <label className="details">Weight (in Kg)</label>
              <input
                type="number"
                step=".01"
                value={member.weight}
                id="weight"
                onChange={(e) => {
                  setMember({ ...member, weight: e.target.value });
                }}
              />
    
              <label className="details">Join Date</label>
              <input
                type="date"
                value={member.join_date}
                id="joindate"
                onChange={(e) => {
                  setMember({ ...member, join_date: e.target.value });
                }}
              />
    
              <label className="details">End Date</label>
              <input
                type="date"
                value={member.end_date}
                id="enddate"
                onChange={(e) => {
                  setMember({ ...member, end_date: e.target.value });
                }}
              />
    
              <input type="submit" value="Update" />
              {/* <input type="submit" value="Back" /> */}
            </div>
            <div style={{ paddingTop: "5px" }}>
              <Link to="/profile" className="pr-btn">
                Back
              </Link>
            </div>
          </form>
        </div>
      );
    } else {
      dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default MemberEditInfo;
