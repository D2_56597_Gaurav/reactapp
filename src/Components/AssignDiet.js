import React, { useState } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import base_url from "../api/bootapi";

function AssignDiet() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  const [diet, setDiet] = useState({});

  if (x > 0) {
    if (userDetails.role == "trainer") {
      const trainerId = userDetails.trainer.trainer_id;
      console.log(trainerId);

      const postDatatoServer = (data) => {
        console.log("response data");
        console.log(data.name);

        axios.post(`${base_url}/saveUpdateDiet`, data).then(
          (response) => {
            console.log("success");
            toast.success("Persons added successfully", {
              position: "bottom-center",
            });
            setDiet({
              diet_id: "",
              trainer_id: "",
              member_id: "",
              dietType: "",
            });
            console.log(diet);
          },
          (error) => {
            console.log(error);
            console.log("error");
            toast.error("Something went wrong", {
              position: "bottom-center",
            });
          }
        );
      };

      //form handler function
      const handleForm = (e) => {
        console.log(diet);
        console.log(diet.diet_id);
        const jsonData = {
          trainer_id: trainerId,
          member_id: diet.member_id,
          dietType: diet.dietType,
        };
        postDatatoServer(jsonData);
        e.preventDefault();
        dashboard();
      };

      return (
        <div id="signup" style={{ height: "100vh", paddingTop: "5%" }}>
          <h1>
            <span>Assign </span>Diet
          </h1>
          <form onSubmit={handleForm}>
            <div>
              <label className="details">Trainer Id</label>
              <input
                id="trainer_id"
                placeholder={trainerId}
                readOnly
              />

              <label className="details">Member Id</label>
              <input
                type="number"
                id="member_id"
                placeholder="Enter Member Id"
                onChange={(e) => {
                  setDiet({ ...diet, member_id: e.target.value });
                }}
                required
              />

              <label className="details">Diet Type</label>
              <select
                id="dietType"
                onChange={(e) => {
                  setDiet({ ...diet, dietType: e.target.value });
                }}
              >
                <option>----Select----</option>
                <option>Type 1(protein shake + fruits)</option>
                <option>Type 2(protein shake + boiled eggs + fruits)</option>
                <option>
                  Type 3(protein shake + boiled eggs + fruits + chicken)
                </option>
                <option>
                  Type 4(protein shake + boiled eggs + fruits + chicken +
                  suppliment)
                </option>
              </select>

              <input type="submit" value="Assign Diet" />
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="/dietinfo" className="pr-btn">
                Back
              </Link>
            </div>
          </form>
        </div>
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default AssignDiet;
