import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import base_url from "../api/bootapi";
import { Link, useParams } from "react-router-dom";

function EditDiet() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const params = useParams();

  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  const [diet, setDiet] = useState({});
  useEffect(() => {
    const fetchData = () => {
      axios.get(`${base_url}/getdiet/${params.id}`).then(
        (response) => {
          setDiet(response.data[0]);
        },
        (error) => {
          console.log(error);
        }
      );
    };
    fetchData();
    // console.log("person"+ person)
  }, []);

  if (x > 0) {
    if (userDetails.role == "trainer") {
      const postDatatoServer = (data) => {
        console.log("response data");
        console.log(data.name);

        axios.put(`${base_url}/saveUpdateDiet`, data).then(
          (response) => {
            console.log("success");
            toast.success("Workout added successfully", {
              position: "bottom-center",
            });
            setDiet({
              diet_id: "",
              trainer_id: "",
              member_id: "",
              dietType: "",
            });
          },
          (error) => {
            console.log(error);
            console.log("error");
            toast.error("Something went wrong", {
              position: "bottom-center",
            });
          }
        );
      };

      //form handler function
      const handleForm = (e) => {
        postDatatoServer(diet);
        e.preventDefault();
        window.location.href = "/diet";
      };

      return (
        <div id="signup" style={{ height: "100vh", paddingTop: "5%" }}>
          <h1>
            <span>Update </span>Diet
          </h1>
          <form onSubmit={handleForm}>
            <div>
            <label className="details">Diet Id</label>
              <input
                type="number"
                id="diet_id"
                placeholder={diet.diet_id}
                readOnly
              />
              <label className="details">Trainer Id</label>
              <input
                id="trainer_id"
                placeholder={diet.trainer_id}
                readOnly
              />

              <label className="details">Member Id</label>
              <input
                type="number"
                id="member_id"
                value={diet.member_id}
                onChange={(e) => {
                  setDiet({ ...diet, member_id: e.target.value });
                }}
                required
              />

              <label className="details">Diet Type</label>
              <select
                id="dietType"
                onChange={(e) => {
                  setDiet({ ...diet, dietType: e.target.value });
                }}
              >
                <option>{diet.dietType}</option>
                <option>Type 1(protein shake + fruits)</option>
                <option>Type 2(protein shake + boiled eggs + fruits)</option>
                <option>
                  Type 3(protein shake + boiled eggs + fruits + chicken)
                </option>
                <option>
                  Type 4(protein shake + boiled eggs + fruits + chicken +
                  suppliment)
                </option>
              </select>

              <input type="submit" value="Assign Diet" />
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="/dietinfo" className="pr-btn">
                Back
              </Link>
            </div>
          </form>
        </div>
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default EditDiet;
