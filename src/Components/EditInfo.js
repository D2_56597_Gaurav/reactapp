import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import base_url from "../api/bootapi";

function EditInfo() {
  const [person, setPerson] = useState([]);
  const navigate = useNavigate();
  const params = useParams();

  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  useEffect(() => {
    const fetchData = () => {
      axios.get(`${base_url}/getperson/${params.id}`).then(
        (response) => {
          setPerson(response.data[0]);
        },
        (error) => {
          console.log(error);
        }
      );
    };
    fetchData();
    // console.log("person"+ person)
  }, []);

  const handleForm = (e) => {
    e.preventDefault();
    if (person.role == "member") {
      const jsonData = {
        id: person.id,
        name: person.name,
        gender: person.gender,
        dob: person.dob,
        age: person.age,
        mobile: person.mobile,
        email: person.email,
        address: person.address,
        username: person.username,
        password: person.password,
        role: person.role,
        member: {
          member_id: person.member.member_id,
          height: person.member.height,
          weight: person.member.weight,
          join_date: person.member.join_date,
          end_date: person.member.end_date,
        },
      };
      postDatatoServer(jsonData);
    } else {
      const jsonData = {
        id: person.id,
        name: person.name,
        gender: person.gender,
        dob: person.dob,
        age: person.age,
        mobile: person.mobile,
        email: person.email,
        address: person.address,
        username: person.username,
        password: person.password,
        role: person.role,
        trainer: {
          trainer_id: person.trainer.trainer_id,
          qualification: person.trainer.qualification,
          specialization: person.trainer.specialization,
          college: person.trainer.college,
          marks: person.trainer.marks,
          salary: person.trainer.salary,
        },
      };

      postDatatoServer(jsonData);
    }
    alert("Update Succesfull..!!");
    navigate("/admin");
  };

  const postDatatoServer = (data) => {
    axios.put(`${base_url}/saveperson`, data).then(
      (response) => {
        console.log("success");
        setPerson({
          email: "",
          password: "",
        });
      },
      (error) => {
        console.log(error);
        console.log("error");
      }
    );
  };

  if (x > 0) {
    if (userDetails.role == "admin") {
      return (
        <div id="signup" style={{ height: "100vh"}}>
          <h1>
            <span>Edit </span>Info
          </h1>
          <form onSubmit={handleForm}>
            <div>
              <label className="details">Email</label>
              <input
                type="email"
                value={person.email}
                id="email"
                pattern="/[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g"
                required
                onChange={(e) => {
                  setPerson({ ...person, email: e.target.value });
                }}
              />

              <label className="details">Password</label>
              <input
                type="password"
                placeholder="Enter new password"
                id="password"
                onChange={(e) => {
                  setPerson({ ...person, password: e.target.value });
                }}
              />

              <input type="submit" value="Update" />
            </div>
            <div style={{ paddingTop: "5px" }}>
              <Link to="/admin" className="pr-btn">
                Back
              </Link>
            </div>
          </form>
        </div>
      );
    } else {
      dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default EditInfo;
