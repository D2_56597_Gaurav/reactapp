import React, { useState, useEffect } from "react";
import "./AllPersons.css";
import { Container, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import base_url from "../api/bootapi";
import axios from "axios";
import { toast } from "react-toastify";

const Workout = () => {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };
  const [workouts, setWorkouts] = useState([]);
  useEffect(() => {
    getAllWorkoutsFromServer();
  }, []);

  const getAllWorkoutsFromServer = () => {
    if (userDetails.role == "member") {
      const memberId = userDetails.member.member_id;
      axios.get(`${base_url}/getWorkoutByMemberId/${memberId}`).then(
        (response) => {
          //debugger
          console.log(response.data);
  
          setWorkouts(response.data);
        },
        (error) => {
          //for error
          console.log(error);
          toast.error("Something went wrong", {
            position: "bottom-center",
          });
        }
      );
    }
    if (userDetails.role == "trainer") {
      const trainerId = userDetails.trainer.trainer_id;
      axios.get(`${base_url}/getWorkoutByTrainerId/${trainerId}`).then(
        (response) => {
          //debugger
          console.log(response.data);

          setWorkouts(response.data);
        },
        (error) => {
          //for error
          console.log(error);
          toast.error("Something went wrong", {
            position: "bottom-center",
          });
        }
      );
    }
    
  };

  if (x > 0) {
    if (userDetails.role == "member") {
      
      const WorkoutLayout = ({ workout, update }) => {
        return (
          <tr>
            <td>{workout.member_id}</td>
            <td>{workout.trainer_id}</td>
            <td>{workout.workout_id}</td>
            <td>{workout.workoutType}</td>
          </tr>
        );
      };
      const updateWorkouts = (id) => {
        setWorkouts(workouts.filter((p) => p.id !== id));
      };

      return (
        <div>
          <div id="login" style={{ color: "White" }}>
            <h1>
              <span>WorkOut</span> Info
            </h1>
            <div>
              <Container>
                <table id="person">
                  <tr>
                    <th>Member Id</th>
                    <th>Trainer Id</th>
                    <th>Workout Id</th>
                    <th>Workout Type</th>
                  </tr>
                  {workouts.length > 0 ? (
                    workouts.map((item) => (
                      <WorkoutLayout
                        key={item.id}
                        workout={item}
                        update={updateWorkouts}
                      />
                    ))
                  ) : (
                    <h3>"No Info Found"</h3>
                  )}
                </table>
              </Container>
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="#" className="pr-btn" onClick={dashboard}>
                Back
              </Link>
            </div>
          </div>
        </div>
      );
    } else if (userDetails.role == "trainer") {
      
      const WorkoutLayout = ({ workout, update }) => {
        return (
          <tr>
            <td>{workout.trainer_id}</td>
            <td>{workout.member_id}</td>
            <td>{workout.workout_id}</td>
            <td>{workout.workoutType}</td>
          </tr>
        );
      };

      const updateWorkouts = (id) => {
        setWorkouts(workouts.filter((p) => p.id !== id));
      };

      return (
        <div>
          <div id="login" style={{ color: "White" }}>
            <h1>
              <span>WorkOut</span> Info
            </h1>
            <div>
              <Container>
                <table id="person">
                  <tr>
                    <th>Trainer Id</th>
                    <th>Member Id</th>
                    <th>Workout Id</th>
                    <th>Workout Type</th>
                  </tr>
                  {workouts.length > 0 ? (
                    workouts.map((item) => (
                      <WorkoutLayout
                        key={item.id}
                        workout={item}
                        update={updateWorkouts}
                      />
                    ))
                  ) : (
                    <h3>"No Info Found"</h3>
                  )}
                </table>
              </Container>
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="#" className="pr-btn" onClick={dashboard}>
                Back
              </Link>
            </div>
          </div>
        </div>
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
};

export default Workout;
