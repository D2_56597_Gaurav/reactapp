import React from "react";
import { Link } from "react-router-dom";
import './Admin.css';

function Trainer() {
  let userDetails = JSON.parse(localStorage.getItem('user'));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  if (x > 0) {
    if (userDetails.role == "trainer") {
      return (
        <div id="login" style={{ height: "100vh", paddingTop: "5%" }}>
        <h1>
          <span>Welcome </span>{userDetails.username}
        </h1>
        <div id="wrapper">
        <article class="card" role="article">
          <Link to="/profile">
            <div class="card-text">
              <div class="card-meta">View</div>
              <h2 class="card-title">Profile</h2>
            </div>
            <img class="card-image" src="https://i0.wp.com/digiday.com/wp-content/uploads/2021/09/editorial-package-illustration-8-privacy-confessions-01.png" />
            </Link>
        </article>
        <article class="card" role="article">
          <Link to="/workoutinfo">
            <div class="card-text">
              <div class="card-meta">View / Assign</div>
              <h2 class="card-title">Workout</h2>
            </div>
            <img class="card-image" src="https://images.pexels.com/photos/791763/pexels-photo-791763.jpeg" />
            </Link>
        </article>
        <article class="card" role="article">
          <Link to="/dietinfo">
            <div class="card-text">
              <div class="card-meta">View / Assign</div>
              <h2 class="card-title">Diet</h2>
            </div>
            <img class="card-image" src="https://ht-one.nl/wp-content/uploads/2019/10/gezond_eten.jpg" />
            </Link>
        </article>
        </div>
      </div>
      
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
  
  }
  
  export default Trainer;