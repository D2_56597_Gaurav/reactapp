import React from "react";
import { Link } from "react-router-dom";
import './Admin.css';

function Admin() {
  let userDetails = JSON.parse(localStorage.getItem('user'));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  if (x>0) {
    if (userDetails.role == "admin") {
      return (
        <div id="login" style={{ height: "100vh", paddingTop: "5%" }}>
        <h1>
          <span>Welcome </span>{userDetails.username}
        </h1>
        <div id="wrapper">
        <article class="card" role="article">
          <Link to="/allpersons">
            <div class="card-text">
              <div class="card-meta"></div>
              <h2 class="card-title">All Persons</h2>
            </div>
            <img class="card-image" src="https://c1.wallpaperflare.com/preview/485/581/97/various-fitness-healthy-sport.jpg" />
            </Link>
        </article>
        <article class="card" role="article">
          <Link to="/alltrainers">
            <div class="card-text">
              <div class="card-meta"></div>
              <h2 class="card-title">All Trainers</h2>
            </div>
            <img class="card-image" src="https://c1.wallpaperflare.com/preview/234/625/249/various-fit-fitness-gym.jpg" />
            </Link>
        </article>
        <article class="card" role="article">
          <Link to="/allmembers">
            <div class="card-text">
              <div class="card-meta"></div>
              <h2 class="card-title">All Members</h2>
            </div>
            <img class="card-image" src="https://c1.wallpaperflare.com/preview/940/41/258/dumbbell-sport-weights-fitness-room.jpg" />
            </Link>
        </article>
        </div>
      </div>
      
      );
    }
    else {dashboard()}
  } else {
    window.location.href = "/login";
  }
    
  }
  
  export default Admin;