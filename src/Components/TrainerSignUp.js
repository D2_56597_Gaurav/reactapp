import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import axios from "axios";
import base_url from "../api/bootapi";

function TrainerSignUp() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  const [person, setPerson] = useState({});
  const navigate = useNavigate();

  if (x > 0) {
    return dashboard();
  } else {
    const postDatatoServer = (data) => {
      console.log("response data");
      console.log(data.name);

      axios.post(`${base_url}/saveperson`, data).then(
        (response) => {
          console.log(response.join_date);
          console.log("success");
          toast.success("Persons added successfully", {
            position: "bottom-center",
          });
          setPerson({
            id: "",
            name: "",
            gender: "",
            dob: "",
            age: "",
            mobile: "",
            email: "",
            address: "",
            username: "",
            password: "",
            role: "",
            qualification: "",
            specialization: "",
            college: "",
            marks: "",
            salary: "",
          });
        },
        (error) => {
          console.log(error);
          console.log("error");
          toast.error("Something went wrong", {
            position: "bottom-center",
          });
        }
      );
    };
    //form handler function
    const handleForm = (e) => {
      console.log(person);
      console.log(person.id);
      console.log(person.name);

      const jsonData = {
        name: person.name,
        gender: person.gender,
        dob: person.dob,
        age: person.age,
        mobile: person.mobile,
        email: person.email,
        address: person.address,
        username: person.username,
        password: person.password,
        role: person.role,
        trainer: {
          qualification: person.qualification,
          specialization: person.specialization,
          college: person.college,
          marks: person.marks,
          salary: person.salary,
        },
      };

      postDatatoServer(jsonData);
      alert("Registeration Succesfull..!!");
      e.preventDefault();
      navigate("/login");
    };

    return (
      <div id="signup">
        <h1>
          <span>Regi</span>ster
        </h1>
        <form onSubmit={handleForm}>
          <div>
            <label className="details">Name</label>
            <input
              type="text"
              id="name"
              placeholder="Name"
              required
              onChange={(e) => {
                setPerson({ ...person, name: e.target.value });
              }}
            />

            <label className="details">Gender</label>
            <select
              id="gender"
              onChange={(e) => {
                setPerson({ ...person, gender: e.target.value });
              }}
            >
              <option></option>
              <option>M</option>
              <option>F</option>
            </select>

            <label className="details">DOB</label>
            <input
              type="date"
              placeholder="DOB"
              id="dob"
              required
              onChange={(e) => {
                setPerson({ ...person, dob: e.target.value });
              }}
            />

            <label className="details">Age</label>
            <input
              type="number"
              placeholder="Age"
              id="age"
              required
              onChange={(e) => {
                setPerson({ ...person, age: e.target.value });
              }}
            />

            <label className="details">Mobile Number</label>
            <input
              type="tel"
              placeholder="Mobile Number"
              id="mobile"
              pattern="[0-9]{10}"
              required
              onChange={(e) => {
                setPerson({ ...person, mobile: e.target.value });
              }}
            />

            <label className="details">Email</label>
            <input
              type="email"
              placeholder="Enter email"
              id="email"
              pattern="/[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g"
              required
              onChange={(e) => {
                setPerson({ ...person, email: e.target.value });
              }}
            />

            <label className="details">Address</label>
            <input
              type="text"
              placeholder="Address"
              id="address"
              required
              onChange={(e) => {
                setPerson({ ...person, address: e.target.value });
              }}
            />

            <label className="details">Username</label>
            <input
              type="text"
              placeholder="Username"
              id="username"
              required
              onChange={(e) => {
                setPerson({ ...person, username: e.target.value });
              }}
            />

            <label className="details">Password</label>
            <input
              type="password"
              placeholder="Enter password"
              id="password"
              required
              onChange={(e) => {
                setPerson({ ...person, password: e.target.value });
              }}
            />

            <label className="details">Role</label>
            <select
              id="role"
              onChange={(e) => {
                setPerson({ ...person, role: e.target.value });
              }}
            >
              <option></option>
              <option>trainer</option>
            </select>

            <label className="details">Qualification</label>
            <input
              type="text"
              placeholder="Qualification"
              id="qualification"
              required
              onChange={(e) => {
                setPerson({ ...person, qualification: e.target.value });
              }}
            />

            <label className="details">Specialization</label>
            <input
              type="text"
              placeholder="Specialization"
              id="specialization"
              required
              onChange={(e) => {
                setPerson({ ...person, specialization: e.target.value });
              }}
            />

            <label className="details">College</label>
            <input
              type="text"
              placeholder="College"
              id="college"
              required
              onChange={(e) => {
                setPerson({ ...person, college: e.target.value });
              }}
            />

            <label className="details">Marks</label>
            <input
              type="number"
              placeholder="Marks"
              id="marks"
              required
              onChange={(e) => {
                setPerson({ ...person, marks: e.target.value });
              }}
            />

            <label className="details">Salary</label>
            <input
              type="number"
              placeholder="Salary"
              id="salary"
              required
              onChange={(e) => {
                setPerson({ ...person, salary: e.target.value });
              }}
            />

            <input type="submit" value="Register" />
          </div>
          <p className="details">
            Already registered? <Link to="/login">LogIn</Link>
          </p>
        </form>
      </div>
    );
  }
}

export default TrainerSignUp;
