import React, { useEffect, useState } from "react";
import base_url from "../api/bootapi";
import { Container, Table } from "react-bootstrap";
import "./AllPersons.css";
import { Link } from "react-router-dom";
import axios from "axios";
import { toast } from "react-toastify";

const AllPersons = () => {
  const [person, setPerson] = useState("");

  let userDetails = JSON.parse(localStorage.getItem('user'));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  useEffect(() => {
    if (userDetails.role == "admin"){
      const url = `${base_url}/getAll`;
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const json = await response.json();
        for (let [i, user] of json.entries()){
          if (user.role === 'admin') {
            // user.slice(i, 1);
            delete json[i];
          }
        }
        setPerson(json);
      } catch (error) {
        console.log("error", error);
      }
    };
    fetchData();
    }
    
  }, []);

  if (x>0) {
    if (userDetails.role == "admin"){
      const Person = ({ person, update }) => {
        const refer = () => {
          if (person.role == 'trainer') {
            window.location.href = "/alltrainers";
          }
          else if (person.role == 'member') {
            window.location.href = "/allmembers";
          }
        }
        const editProfile = () => {
            window.location.href = "/editinfo/" + person.id;
          }
        const deletePerson = (id) => {
          axios.delete(`${base_url}/deleteperson/${id}`).then(
            (response) => {
              toast.success("person deleted");
              window.location.reload();
            },
            (error) => {
              toast.error("person not deleted..!!! Server Problem");
            }
          );
        };
        return (
            <tr>
              <td onClick={refer}>{person.id}</td>
              <td onClick={refer}>{person.name}</td>
              <td onClick={refer}>{person.gender}</td>
              <td onClick={refer}>{person.dob}</td>
              <td onClick={refer}>{person.age}</td>
              <td onClick={refer}>{person.mobile}</td>
              <td onClick={refer}>{person.email}</td>
              <td onClick={refer}>{person.address}</td>
              <td onClick={refer}>{person.username}</td>
              <td onClick={refer}>{person.role}</td>
              <td>
                {" "}
                <button
                  color="danger"
                  onClick={() => {
                    deletePerson(person.id);
                  }}
                >
                  Delete
                </button>
              </td>
              <td>
                {" "}
                <button onClick={editProfile}>
                  Edit Info
                </button>
              </td>
            </tr>
        );
      };
      
    
      return (
        <div>
          <div id="login" style={{ color: "White" }}>
            <h1>
              <span>All Persons info</span> are as follows:
            </h1>
            <div>
              <Container>
                <table id="person">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>DOB</th>
                    <th>Age</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th colspan="2">Actions</th>
                  </tr> 
                    {person.length > 0 ? (
                    person.map((item) => <Person person={item} />)
                  ) : (
                    <h3>"No Info Found"</h3>
                  )}
                  
                  
                </table>
              </Container>
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="/admin" className="pr-btn">
                Back
              </Link>
            </div>
          </div>
        </div>
      );
    } else {
      dashboard();
    }
  } else {
    window.location.href = "/login";
  }
  
};

export default AllPersons;
