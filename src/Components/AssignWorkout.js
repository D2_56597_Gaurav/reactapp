import React, { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import base_url from "../api/bootapi";
import { Link } from "react-router-dom";

function AssignWorkout() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;

  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  const [workout, setWorkout] = useState({});

  if (x > 0) {
    if (userDetails.role == "trainer") {
      const trainerId = userDetails.trainer.trainer_id;
      const postDatatoServer = (data) => {
        console.log("response data");
        console.log(data.name);

        axios.post(`${base_url}/saveUpdateWorkout`, data).then(
          (response) => {
            console.log("success");
            toast.success("Workout added successfully", {
              position: "bottom-center",
            });
            setWorkout({
              workout_id: "",
              trainer_id: "",
              member_id: "",
              workoutType: "",
            });
          },
          (error) => {
            console.log(error);
            console.log("error");
            toast.error("Something went wrong", {
              position: "bottom-center",
            });
          }
        );
      };

      //form handler function
      const handleForm = (e) => {
        console.log(workout);
        console.log(workout.workout_id);
        const jsonData = {
          trainer_id: trainerId,
          member_id: workout.member_id,
          workoutType: workout.workoutType,
        };
        postDatatoServer(jsonData);
        e.preventDefault();
        dashboard();
      };

      return (
        <div id="signup" style={{ height: "100vh", paddingTop: "5%" }}>
          <h1>
            <span>Assign </span>Workout
          </h1>
          <form onSubmit={handleForm}>
            <div>
              <label className="details">Trainer Id</label>
              <input
                id="trainer_id"
                placeholder={trainerId}
                readOnly
              />

              <label className="details">Member Id</label>
              <input
                type="number"
                id="member_id"
                placeholder="Enter Member Id"
                onChange={(e) => {
                  setWorkout({ ...workout, member_id: e.target.value });
                }}
                required
              />

              <label className="details">Workout Type</label>
              <select
                id="workoutType"
                onChange={(e) => {
                  setWorkout({ ...workout, workoutType: e.target.value });
                }}
                required
              >
                <option>----Select----</option>
                <option>Type 1( warmup + pushup + cardio 5min)</option>
                <option>
                  Type 2(warmup + pushup +upperbody+ cardio 10 min)
                </option>
                <option>
                  Type 3(warmup + pushup +upperbody+ lowerbody+ cardio 10 min)
                </option>
                <option>Type 4(warmup + 1 day 3 parts + cardio 15 min)</option>
              </select>

              <input type="submit" value="Assign Workout" />
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="/workoutinfo" className="pr-btn">
                Back
              </Link>
            </div>
          </form>
        </div>
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default AssignWorkout;
