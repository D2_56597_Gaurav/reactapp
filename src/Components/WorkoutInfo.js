import React from "react";
import { Link } from "react-router-dom";

function WorkoutInfo() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  if (x > 0) {
    if (userDetails.role == "trainer") {
      return (
        <div id="signup" style={{ height: "100vh", paddingTop: "200px" }}>
          <h1>
            <span>View/Assign</span> Workout
          </h1>
          <div>
            <Link to="/assignworkout" className="pr-btn">
              AssignWorkout
            </Link>
          </div>
          <> </>
          <div style={{ marginTop: "10px" }}>
            <Link to="/workout" className="pr-btn">
              View Workout
            </Link>
          </div>
          <div style={{ paddingTop: "10px" }}>
            <Link to="#" className="pr-btn" onClick={dashboard}>
              Back
            </Link>
          </div>
        </div>
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default WorkoutInfo;
