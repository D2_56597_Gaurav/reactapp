import React, { useState, useEffect } from "react";
import "./AllPersons.css";
import { Container, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import base_url from "../api/bootapi";
import axios from "axios";
import { toast } from "react-toastify";

const Diet = () => {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  const [diets, setDiets] = useState([]);
  useEffect(() => {
    getAllDietsFromServer();
  }, []);

  const getAllDietsFromServer = () => {
    if (userDetails.role == "member") {
      const memberId = userDetails.member.member_id;
      axios.get(`${base_url}/getDietByMemberId/${memberId}`).then(
        (response) => {
          //debugger
          console.log(response.data);
  
          setDiets(response.data);
        },
        (error) => {
          //for error
          console.log(error);
          toast.error("Something went wrong", {
            position: "bottom-center",
          });
        }
      );
    }
    if (userDetails.role == "trainer") {
      const trainerId = userDetails.trainer.trainer_id;
      axios.get(`${base_url}/getDietByTrainerId/${trainerId}`).then(
        (response) => {
          //debugger
          console.log(response.data);

          setDiets(response.data);
        },
        (error) => {
          //for error
          console.log(error);
          toast.error("Something went wrong", {
            position: "bottom-center",
          });
        }
      );
    }
    
  };

  if (x > 0) {
    if (userDetails.role == "member") {
      const DietLayout = ({ diet, update }) => {
        return (
          <tr>
            <td>{diet.member_id}</td>
            <td>{diet.trainer_id}</td>
            <td>{diet.diet_id}</td>
            <td>{diet.dietType}</td>
          </tr>
        );
      };

      const updateDiets = (id) => {
        setDiets(diets.filter((p) => p.id !== id));
      };

      return (
        <div>
          <div id="login" style={{ color: "White" }}>
            <h1>
              <span>Diet</span> Info
            </h1>
            <div>
              <Container>
                <table id="person">
                  <tr>
                    <th>Member Id</th>
                    <th>Trainer Id</th>
                    <th>Diet Id</th>
                    <th>Diet Type</th>
                  </tr>
                  {diets.length > 0 ? (
                    diets.map((item) => (
                      <DietLayout
                        key={item.id}
                        diet={item}
                        update={updateDiets}
                      />
                    ))
                  ) : (
                    <h3>"No Info Found"</h3>
                  )}
                </table>
              </Container>
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="#" className="pr-btn" onClick={dashboard}>
                Back
              </Link>
            </div>
          </div>
        </div>
      );
    } else if (userDetails.role == "trainer") {
      const DietLayout = ({ diet, update }) => {
        return (
          <tr>
            <td>{diet.trainer_id}</td>
            <td>{diet.member_id}</td>
            <td>{diet.diet_id}</td>
            <td>{diet.dietType}</td>
            <td>
                {" "}
                <button
                  color="danger"
                  onClick={() => {
                    deleteDiet(diet.diet_id);
                  }}
                >
                  Delete
                </button>
              </td>
              <td>
                {" "}
                <button onClick={() => {
                    editDiet(diet.diet_id);
                  }}>
                  Edit
                </button>
              </td>
          </tr>
        );
      };

      const editDiet = (id) => {
        window.location.href = "/editdiet/" +id;
      }

      const deleteDiet = (id) => {
        axios.delete(`${base_url}/deletediet/${id}`).then(
          (response) => {
            toast.success("workout deleted");
            window.location.reload();
          },
          (error) => {
            toast.error("workout not deleted..!!! Server Problem");
          }
        );
      };

      const updateDiets = (id) => {
        setDiets(diets.filter((p) => p.id !== id));
      };

      return (
        <div>
          <div id="login" style={{ color: "White" }}>
            <h1>
              <span>Diet</span> Info
            </h1>
            <div>
              <Container>
                <table id="person">
                  <tr>
                    <th>Trainer Id</th>
                    <th>Member Id</th>
                    <th>Diet Id</th>
                    <th>Diet Type</th>
                    <th colspan="2" style={{textAlign:"center"}}>Action</th>
                  </tr>
                  {diets.length > 0 ? (
                    diets.map((item) => (
                      <DietLayout
                        key={item.id}
                        diet={item}
                        update={updateDiets}
                      />
                    ))
                  ) : (
                    <h3>"No Info Found"</h3>
                  )}
                </table>
              </Container>
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="/dietinfo" className="pr-btn">
                Back
              </Link>
            </div>
          </div>
        </div>
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
};

export default Diet;
