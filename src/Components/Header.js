import React from "react";
import Feature from "./Feature";
import Offer from "./Offer";
import About from "./About";
import Contact from "./Contact";
import { Link } from "react-router-dom";

function Header() {
  return (
      <div>
    <div class="row">
    <div id="main">
      <div className="pr-heading">
        <h2>START YOUR FITNESS LIFE</h2>
        <h1>
          <span>BY</span> JOINING US
        </h1>
        <p className="details">
          Build your Body and Fitness with professional Touch
        </p>
        <div className="header-btns">
          <Link to="/signup" className="header-btn" action>
            JOIN US
          </Link>
        </div>
      </div>
      </div>
      <div class="row">
      <Feature />
      <Offer />
      <About />
      <Contact />
    </div>
    </div>
    </div>
  );
}

export default Header;
