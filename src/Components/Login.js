import React, { Component, useState, useContext } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "./UserContext";
import ApiService from "../api/ApiService";

const Login = (props) => {
  const { setUser } = useContext(UserContext);
  const [login, setLogin] = useState();
  const [error, setError] = useState();

  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  if (x > 0) {
    dashboard();
  } else {
    const setData = (event) => {
      let data = event.target.value;
      let id = event.target.id;
      if (id === "password") {
        setLogin((prevData) => ({ ...prevData, password: data }));
      } else if (id === "email") {
        setLogin((prevData) => ({ ...prevData, email: data }));
      }
    };

    const handleSignIn = (e) => {
      e.preventDefault();
      ApiService.login(login)
        .then((resp) => {
          let user = resp.data;
          setUser(user);
          localStorage.setItem("user", JSON.stringify(user));
          setError(null);
          if (userDetails.role == "admin") {
            window.location.href = "/admin";
          } else if (userDetails.role == "trainer") {
            window.location.href = "/trainer";
          } else if (userDetails.role == "member") {
            window.location.href = "/member";
          } else if (user.length < 1) {
            alert("Please Enter Valid Data");
          } 
        })
        .catch(() => {
          setUser(null);
          setError("Invalid Login!");
        });
    };
    return (
      <div id="login">
        <h1>
          <span>Log</span>In
        </h1>
        <form>
          <div>
            <label className="details" htmlFor="email">
              Email :
            </label>
            <input
              type="email"
              id="email"
              placeholder="Enter email"
              required
              onChange={setData}
            />
            <label className="details" htmlFor="password">
              Password :
            </label>
            <input
              type="password"
              id="password"
              placeholder="Enter password"
              required
              onChange={setData}
            />
            <input type="submit" value="Sign In" onClick={handleSignIn} />
            <p className="details">
              Not a member yet? <Link to="/signup"> SignUp</Link>
            </p>
          </div>
        </form>
      </div>
    );
  }
};

export default Login;
