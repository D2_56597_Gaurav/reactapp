import React, { useEffect, useState } from "react";
import Profilelayout from "./Profilelayout";
import base_url from "../api/bootapi";
import { Container, Table } from "react-bootstrap";
import "./AllPersons.css";
import { Link } from "react-router-dom";
import axios from "axios";
import { toast } from "react-toastify";

const Profile = () => {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const [persons, setPersons] = useState([]);
  useEffect(() => {
    getAllPersonsFromServer();
  }, []);

  const personId = userDetails.id;

  const getAllPersonsFromServer = () => {
    axios.get(`${base_url}/getperson/${personId}`).then(
      (response) => {
        //debugger
        console.log(response.data);
        setPersons(response.data);
      },
      (error) => {
        //for error
        console.log(error);
        toast.error("Something went wrong", {
          position: "bottom-center",
        });
      }
    );
  };

  if (x > 0) {
    const dashboard = () => {
      if (userDetails.role == "admin") {
        window.location.href = "/admin";
      } else if (userDetails.role == "trainer") {
        window.location.href = "/trainer";
      } else if (userDetails.role == "member") {
        window.location.href = "/member";
      }
    };

    const updatePersons = (id) => {
      setPersons(persons.filter((p) => p.id !== id));
    };

    if (userDetails.role == "member") {
      return (
        <div>
          <div id="login" style={{ color: "White" }}>
            <h1>
              <span>Profile</span> Info
            </h1>
            <div>
              <Container>
                <table id="person">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>DOB</th>
                    <th>Age</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Username</th>
                    <th>Member Id</th>
                    <th>Height</th>
                    <th>Weight</th>
                    <th>Join Date</th>
                    <th>End Date</th>
                  </tr>
                  {persons.length > 0 ? (
                    persons.map((item) => <Profilelayout person={item} />)
                  ) : (
                    <h3>"No Info Found"</h3>
                  )}
                </table>
              </Container>
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to={"/membereditinfo/" + personId} className="pr-btn">
                {" "}
                Edit Info
              </Link>
            </div>
            <div style={{ paddingTop: "10px" }}>
              <Link to="#" className="pr-btn" onClick={dashboard}>
                Back
              </Link>
            </div>
          </div>
        </div>
      );
    } else if (userDetails.role == "trainer") {
      return (
        <div>
          <div id="login" style={{ color: "White" }}>
            <h1>
              <span>Profile</span> Info
            </h1>
            <div>
              <Container>
                <table id="person">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>DOB</th>
                    <th>Age</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Username</th>
                    <th>Trainer Id</th>
                    <th>Qualification</th>
                    <th>Specialization</th>
                    <th>College</th>
                    <th>Marks</th>
                    <th>Salary</th>
                  </tr>
                  {persons.length > 0 ? (
                    persons.map((item) => <Profilelayout person={item} />)
                  ) : (
                    <h3>"No Info Found"</h3>
                  )}
                </table>
              </Container>
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to={"/trainereditinfo/" + personId} className="pr-btn">
                {" "}
                Edit Info
              </Link>
            </div>
            <div style={{ paddingTop: "10px" }}>
              <Link to="#" className="pr-btn" onClick={dashboard}>
                Back
              </Link>
            </div>
          </div>
        </div>
      );
    } else return dashboard();
  } else {
    window.location.href = "/login";
  }
};

export default Profile;
