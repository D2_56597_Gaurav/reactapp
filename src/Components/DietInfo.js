import React from "react";
import { Link } from "react-router-dom";

function DietInfo() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  if (x > 0) {
    if (userDetails.role == "trainer") {
      return (
        <div id="signup" style={{ height: "100vh", paddingTop: "200px" }}>
          <h1>
            <span>View/Assign</span> Diet
          </h1>
          <div>
            <Link to="/assigndiet" className="pr-btn">
              Assign Diet
            </Link>
          </div>
          <> </>
          <div style={{ marginTop: "10px" }}>
            <Link to="/diet" className="pr-btn">
              View Diet
            </Link>
          </div>
          <div style={{ paddingTop: "10px" }}>
            <Link to="#" className="pr-btn" onClick={dashboard}>
              Back
            </Link>
          </div>
        </div>
      );
    } else {
      return dashboard();
    }
  } else {
    window.location.href = "/login";
  }
}

export default DietInfo;
