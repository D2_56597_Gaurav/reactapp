import React from "react";
import { Link } from "react-router-dom";

function SignUp() {
  let userDetails = JSON.parse(localStorage.getItem("user"));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "admin") {
      window.location.href = "/admin";
    } else if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  if (x > 0) {
    return dashboard();
  } else {
    return (
      <div id="signup" style={{ height: "100vh", paddingTop: "200px" }}>
        <h1>
          <span>Regi</span>ster <span>A</span>s?
        </h1>
        <div>
          <Link to="/membersignup" className="pr-btn">
            Member
          </Link>
        </div>
        <> </>
        <div style={{ marginTop: "10px" }}>
          <Link to="/trainersignup" className="pr-btn">
            Trainer
          </Link>
        </div>
        <div>
          <p className="details">
            Already registered? <Link to="/login">LogIn</Link>
          </p>
        </div>
      </div>
    );
  }
  
}

export default SignUp;
