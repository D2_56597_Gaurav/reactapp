import React, { useEffect, useState } from "react";
import base_url from "../api/bootapi";
import { Container, Table } from "react-bootstrap";
import "./AllPersons.css";
import { Link } from "react-router-dom";
import axios from "axios";
import { toast } from "react-toastify";

const AllMembers = () => {
  const [person, setPerson] = useState("");

  let userDetails = JSON.parse(localStorage.getItem('user'));
  var x = localStorage.length;
  const dashboard = () => {
    if (userDetails.role == "trainer") {
      window.location.href = "/trainer";
    } else if (userDetails.role == "member") {
      window.location.href = "/member";
    }
  };

  useEffect(() => {
    if (userDetails.role == "admin") {
      const url = `${base_url}/getpersonasmember`;
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const json = await response.json();
        console.log(json);
        setPerson(json);
      } catch (error) {
        console.log("error", error);
      }
    };
    fetchData();
    }
  }, []);

  if (x>0) {
    if (userDetails.role == "admin") {
      const MemberInfo = ({ person, member }) => {
        const deletePerson = (id) => {
          axios.delete(`${base_url}/deleteperson/${id}`).then(
            (response) => {
              toast.success("person deleted");
              window.location.reload();
            },
            (error) => {
              toast.error("person not deleted..!!! Server Problem");
            }
          );
        };
        const editProfile = () => {
          window.location.href = "/editinfo/" + person.id;
        }
        return (
            <tr>
              <td>{person.id}</td>
              <td>{person.name}</td>
              <td>{person.gender}</td>
              <td>{person.dob}</td>
              <td>{person.age}</td>
              <td>{person.mobile}</td>
              <td>{person.email}</td>
              <td>{person.address}</td>
              <td>{person.username}</td>
              <td>{person.role}</td>
              <td>{person.member.member_id}</td>
              <td>{person.member.height}</td>
              <td>{person.member.weight}</td>
              <td>{person.member.join_date}</td>
              <td>{person.member.end_date}</td>
              <td>
                {" "}
                <button
                  color="danger"
                  onClick={() => {
                    deletePerson(person.id);
                  }}
                >
                  Delete
                </button>
              </td>
              <td>
                {" "}
                <button onClick={editProfile}>
                  Edit Info
                </button>
              </td>
            </tr>
        );
      };
      
    
      return (
        <div>
          <div id="login" style={{ color: "White" }}>
            <h1>
              <span>All Members info</span> are as follows:
            </h1>
            <div>
              <Container>
                <table id="person">
                  <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>DOB</th>
                    <th>Age</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th>Member Id</th>
                    <th>Height</th>
                    <th>Weight</th>
                    <th>Join Date</th>
                    <th>End Date</th>
                    <th colspan="2">Actions</th>
                  </tr>
                  {person.length > 0 ? (
                    person.map((item) => <MemberInfo person={item} />)
                  ) : (
                    <h3>"No Info Found"</h3>
                  )}
                </table>
              </Container>
            </div>
            <div style={{ paddingTop: "15px" }}>
              <Link to="/admin" className="pr-btn">
                Back
              </Link>
            </div>
          </div>
        </div>
      );
    }
    else {
      dashboard();
    }
  } else {
    window.location.href = "/login";
  }

  
};

export default AllMembers;
